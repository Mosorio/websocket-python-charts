from flask import Flask
from flask import jsonify
import json
from flask import json
import pymysql
from flask_cors import CORS


app = Flask(__name__)
CORS(app)

@app.route('/')
def home():
   return 'Index API-REST' 


@app.route ('/Grafico')
def getdataJugador():
   # Connect to the database
   connection = pymysql.connect(host='192.168.2.161',
                               user='Qatest',
                               password='Quito.2019',
                               db='automatizacion',
                               charset='utf8mb4',
                               cursorclass=pymysql.cursors.DictCursor)
 
   try:
       with connection.cursor() as cursor:
           # Read a single record
           sql = "SELECT COUNT(Genero) AS Total FROM `json_metrics` "
           cursor.execute(sql)
           result = cursor.fetchall()
           print(result)
           #///////////////////////////////
           sql2 = "SELECT COUNT(Genero) AS Male FROM `json_metrics` WHERE `Id_Genero`=%s"
           cursor.execute(sql2, ('1'))
           resultMale = cursor.fetchall()
           print(resultMale)
           #///////////////////////////////
           sql3 = "SELECT COUNT(Id_Genero) AS Female FROM `json_metrics` WHERE `Id_Genero`=%s"
           cursor.execute(sql3, ('2'))
           resultFemale = cursor.fetchall()
           print(resultFemale)
       return jsonify(
           #show all data
           {"Total Personas": result, "Male": resultMale, "Female": resultFemale},
           {"Message": "Datos Totales por Genero"}
           #show one result of data
           #{"Data": result[0], "message": "datos Web Change"}
           )
   finally:
       connection.close()
@app.route('/test')
def test():
    # Connect to the database
    connection = pymysql.connect(host='192.168.2.161',
                                user='Qatest',
                                password='Quito.2019',
                                db='automatizacion',
                                charset='utf8mb4',
                                cursorclass=pymysql.cursors.DictCursor)

    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT COUNT(Genero) AS Total FROM `json_metrics` "
            cursor.execute(sql)
            result = cursor.fetchall()
            print(result)
            #///////////////////////////////
            sql2 = "SELECT COUNT(Genero) AS Male FROM `json_metrics` WHERE `Id_Genero`=%s"
            cursor.execute(sql2, ('1'))
            resultMale = cursor.fetchall()
            male = resultMale[0]['Male']
            print("Male: ", male)
            #print(resultMale)
            #///////////////////////////////
            sql3 = "SELECT COUNT(Id_Genero) AS Female FROM `json_metrics` WHERE `Id_Genero`=%s"
            cursor.execute(sql3, ('2'))
            resultFemale = cursor.fetchall()
            Female = resultFemale[0]['Female']
            print("Female: ", Female)  
            #print(resultFemale)   

        return jsonify({
            "cols": [
                {"id":"","label":"Topping","pattern":"","type":"string"},
                {"id":"","label":"Slices","pattern":"","type":"number"}
                ],
                "rows": [
                {"c":[{"v":"Male","f":"Male"},{"v":male,"f":male}]},
                {"c":[{"v":"Female","f":"Female"},{"v":Female,"f":Female}]}
                ]
                })
    finally:
        connection.close()

if __name__ == '__main__':
   #app.run(debug=True, port=5050)
   app.run(host='192.168.2.161', debug=True, port=5060)
   #app.run(debug=True, port=5050)
   
#coemtario
 
