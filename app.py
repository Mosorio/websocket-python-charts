from flask import Flask
from flask import jsonify
import json
from flask import json
import pymysql


app = Flask(__name__)


@app.route('/')
def home():
   return 'Index API-REST' 

@app.route ('/Pais')
def getpais():
   # Connect to the database
   connection = pymysql.connect(host='192.168.2.161',
                               user='qatest3',
                               password='Quito.2019',
                               db='K-360',
                               charset='utf8mb4',
                               cursorclass=pymysql.cursors.DictCursor)
 
   try:
       with connection.cursor() as cursor:
           # Read a single record
           sql = "SELECT * FROM `Pais` order by Id"
           cursor.execute(sql)
           result = cursor.fetchall()
           print(result)
       return jsonify(
           #show all data
           {"Data": result, "message": "Lista de Paises"}
           #show one result of data
           #{"Data": result[0], "message": "datos Web Change"}
           )
   finally:
       connection.close()

@app.route ('/Data')
def getdata():
   # Connect to the database
   connection = pymysql.connect(host='192.168.2.161',
                               user='qatest3',
                               password='Quito.2019',
                               db='testflask',
                               charset='K-360',
                               cursorclass=pymysql.cursors.DictCursor)
 
   try:
       with connection.cursor() as cursor:
           # Read a single record
           sql = "SELECT `endpointid`, `hostname` FROM `test` WHERE `endpointid`=%s"
           cursor.execute(sql, ('1234'))
           result = cursor.fetchall()
           print(result)
       return jsonify(
           #show all data
           {"Data": result, "message": "Lista de Endpoints"}
           #show one result of data
           #{"Data": result[0], "message": "datos Web Change"}
           )
   finally:
       connection.close()

@app.route ('/Grafico')
def getdataJugador():
   # Connect to the database
   connection = pymysql.connect(host='192.168.2.161',
                               user='Qatest',
                               password='Quito.2019',
                               db='automatizacion',
                               charset='utf8mb4',
                               cursorclass=pymysql.cursors.DictCursor)
 
   try:
       with connection.cursor() as cursor:
           # Read a single record
           sql = "SELECT COUNT(Genero) AS Total FROM `json_metrics` "
           cursor.execute(sql)
           result = cursor.fetchall()
           print(result)
           #///////////////////////////////
           sql2 = "SELECT COUNT(Genero) AS Male FROM `json_metrics` WHERE `Id_Genero`=%s"
           cursor.execute(sql2, ('1'))
           resultMale = cursor.fetchall()
           print(resultMale)
           #///////////////////////////////
           sql3 = "SELECT COUNT(Id_Genero) AS Female FROM `json_metrics` WHERE `Id_Genero`=%s"
           cursor.execute(sql3, ('2'))
           resultFemale = cursor.fetchall()
           print(resultFemale)
       return jsonify(
           #show all data
           {"Total Personas": result, "Male": resultMale, "Female": resultFemale},
           {"Message": "Datos Totales por Genero"}
           #show one result of data
           #{"Data": result[0], "message": "datos Web Change"}
           )
   finally:
       connection.close()
@app.route('/test')
def test():
   return 'Pull change data 2'

if __name__ == '__main__':
   #app.run(debug=True, port=5050)
   app.run(host='192.168.2.161', debug=True, port=5050)
   #app.run(debug=True, port=5050)
   
#coemtario
 
